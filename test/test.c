/* An example application for GUniqueApp
 * Copyright (C) 2006  Vytautas Liuolia
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <config.h>
#include <stdlib.h>
#include <gtk/gtk.h>

#include "guniqueapp.h"

GtkWidget *window;

void destroy (GtkWidget *widget, gpointer data)
{
	gtk_main_quit ();
}

static void my_callback (GUniqueApp *app, GUniqueAppCommand command, gchar* data, gchar* startup_id, guint workspace, gpointer user_data)
{
	g_print ("Command received:\n\tCommand: %i\n\tData: %s\n\tStartup ID: %s\n\tWorkspace: %u\n", 
		command, data, startup_id, workspace);
		
	/* Here we simulate various behaviour:
	 * opening a new document window, or just activating the existing one)
	 */
	if (command == G_UNIQUE_APP_NEW)
	{
		GtkWidget *win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
		gtk_window_set_title (GTK_WINDOW (win), "New window");
		gtk_window_set_startup_id (GTK_WINDOW (win), startup_id);
		gtk_widget_show_all (win);
	}
		
	else if (command == G_UNIQUE_APP_OPEN || command == G_UNIQUE_APP_ACTIVATE)
	{
		if (command == G_UNIQUE_APP_OPEN) 
			gtk_label_set_label (GTK_LABEL (user_data), data);	
		gtk_window_set_startup_id (GTK_WINDOW (window), startup_id);
	}
	
	else if (command == G_UNIQUE_APP_CUSTOM)
	{
		gchar** cmd = g_strsplit (data, "\v", 0);
		gchar* text = g_strjoinv ("\nArgument: ", cmd);
		gtk_label_set_label (GTK_LABEL (user_data), text);
		
		gtk_window_set_startup_id (GTK_WINDOW (window), startup_id);
		
		g_free (text);
		g_strfreev (cmd);
	}
}

int main (int argc, char** argv)
{
	const gchar* startup_id = NULL;
	
#ifdef OLDER_GTK_VERSION
	startup_id = g_getenv ("DESKTOP_STARTUP_ID");
#endif

	gtk_init (&argc, &argv);
	
	/* testing stuff */
	GUniqueApp *app = g_unique_app_get_with_startup_id ("guniqueapp_test", startup_id);
	
	/* Here we make the application lag on startup, much like OpenOffice ;-) 
	   Useful for investigating focus. comment out if not needed */
	/* g_usleep (G_USEC_PER_SEC* 4); */
	
	if (g_unique_app_is_running (app))
	{		
		if (argc < 2) g_unique_app_new_document (app);
		else if (argc == 2) g_unique_app_open_uri (app, argv[1]);
		else 
		{
			gchar* data = g_strjoinv ("\v", argv);
			g_unique_app_custom_message (app, data);
			g_free (data);
		}
		
		/* This is not needed with patched GTK */
#ifdef OLDER_GTK_VERSION
		gdk_notify_startup_complete (); 
#endif
		
		g_object_unref (app);
		exit(1);
	}

	/* window */
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	g_signal_connect (G_OBJECT(window), "destroy", G_CALLBACK (destroy), NULL);
	gtk_window_set_title (GTK_WINDOW (window), "Very simple single-instance app");
	gtk_window_set_default_size (GTK_WINDOW (window), 400, 150);
	gtk_container_set_border_width (GTK_CONTAINER (window), 4);
	
	/* labels */
	GtkWidget *label1 = gtk_label_new("<b>This application is a pseudo-viewer which\nonly shows the given filename below:</b>");
	gtk_label_set_use_markup (GTK_LABEL (label1), TRUE);
	GtkWidget *label2 = gtk_label_new (NULL);
	
	/* Parsing the arguments */
	if (argc > 1) 
		gtk_label_set_label (GTK_LABEL (label2), argv[1]);
		
	g_signal_connect (G_OBJECT (app), "message", (GCallback) my_callback, label2);
	
	/* box */
	GtkWidget *box = gtk_vbox_new (TRUE, 4);
	gtk_container_add (GTK_CONTAINER (box), label1);
	gtk_container_add (GTK_CONTAINER (box), label2);
	gtk_container_add (GTK_CONTAINER (window), box);
	
	/* run */
	gtk_widget_show_all (window);
	gtk_main ();
	
	g_object_unref(app);
    
	return 0;
}
